const food = document.querySelector('.lista-comida');
const tbody = document.querySelector('tbody');
eventListener();
function eventListener(){
    food.addEventListener('click', buyFood);
    document.querySelector('.vaciar-carrito').addEventListener('click', deleteAll);
}
function buyFood(e){
    e.preventDefault();
    if(e.target.classList.contains('agregar-carrito')){
        const food = e.target.parentElement.parentElement;
        listFood(food);
    }
}
function listFood(food){
    const foodInformation = {
        image : food.querySelector('img').src,
        name : food.querySelector('.card-title').innerText,
        price : food.querySelector('span').textContent,
        id : food.querySelector('a').getAttribute('data-id')
    }
    saveFoodInformation(foodInformation);
}
function saveFoodInformation(food){
    let foods;
    const tr = document.createElement('tr');
    tr.innerHTML = `
        <td>
            <img src="${food.image}"/> 
        </td>
        <td>${food.name}</td>
        <td>${food.price}</td>
        <td><a href="#" data-id="${food.id}" onclick="deleteFood('${food.id}')">X</a></td>`;
    tbody.appendChild(tr);
    foods = obtainLocalStorage();
    foods.push(food);
    localStorage.setItem('food', JSON.stringify(foods));
}
function obtainLocalStorage(){
    let food;
    if(localStorage.getItem('food') === null){
        food = [];
    } else {
        food = JSON.parse(localStorage.getItem('food'));
    }
    return food;
}
function deleteFood(id){
    let food;
    food = obtainLocalStorage();
    food.forEach((element, index) => {
        if(element.id === id){
            console.log(element);
            food.splice(index, 1);
            console.log(food);
        }
    });
    localStorage.setItem('food', JSON.stringify(food));
    loadedFood();
}
loadedFood();
function loadedFood(){
    let food;
    food = obtainLocalStorage();
    tbody.innerHTML = '';
    food.forEach(element => {
        const tr = document.createElement('tr');
        tr.innerHTML = `
        <td>
            <img src="${element.image}"/> 
        </td>
        <td>${element.name}</td>
        <td>${element.price}</td>
        <td><a href="#" data-id="${element.id}" onclick="deleteFood('${element.id}')">X</a></td>`;
        tbody.appendChild(tr);
    });
}
function deleteAll(){
    localStorage.clear();
    loadedFood();
}